from django.conf import settings
from influxdb.client import InfluxDBClient


def get_influx():
    return InfluxDBClient(database=settings.DB_NAME)

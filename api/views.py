
from django.conf import settings
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.mixins import CreateModelMixin
from drf_ujson.renderers import UJSONRenderer

from sososa.models import Race, Team, influx_client

from .serializers import RaceSerializer, TeamSerializer, LocationSerializer


MAX_PAGE_SIZE = 200


class RaceViewSet(ModelViewSet):
    queryset = Race.objects.all()
    serializer_class = RaceSerializer


class TeamViewSet(ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
    filter_fields = ('race',)

    def get_serializer_context(self):
        ctx = super().get_serializer_context()
        if self.request.GET.get('with_location'):
            ctx['with_location'] = True
        return ctx


class LocationViewSet(CreateModelMixin, GenericViewSet):
    '''
    This viewset is meant to return only latest Condition records.
    '''
    serializer_class = LocationSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    # Don't include BrowsableAPIRenderer, because this view doesn't use Django model
    renderer_classes = (UJSONRenderer,)
    filter_fields = ('team',)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_page_size(self, request):
        default = settings.REST_FRAMEWORK['PAGE_SIZE']
        page_size = self.request.query_params.get(
            'page_size', default)
        try:
            page_size = min(int(page_size), MAX_PAGE_SIZE)
        except ValueError:
            page_size = default
        return page_size

    def build_where_clause(self, request):
        keys = ('team_id',)
        where = []
        for k in keys:
            value = request.query_params.getlist(k)
            if value:
                conds = ("{} = '{}'".format(k, v) for v in value)
                where.append('({})'.format(' OR '.join(conds)))
        if where:
            return 'WHERE ' + ' AND '.join(where)
        return ''

    def list(self, request, *args, **kwargs):
        page_size = self.get_page_size(request)
        # Build query command
        q = "SELECT * FROM location {} ORDER BY time DESC LIMIT {}".format(
            self.build_where_clause(request), page_size)
        rs = influx_client.query(q)
        serializer = self.get_serializer(rs.get_points(), many=True)
        return Response({'results': serializer.data})

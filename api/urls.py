from rest_framework.routers import DefaultRouter

from .views import RaceViewSet, TeamViewSet, LocationViewSet

router = DefaultRouter(trailing_slash=False)
router.register('races', RaceViewSet)
router.register('teams', TeamViewSet)
router.register('locations', LocationViewSet, 'location')

urlpatterns = router.urls

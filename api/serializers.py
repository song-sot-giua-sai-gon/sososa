from django.utils import timezone
from rest_framework import serializers
from rest_framework.serializers import Serializer, ModelSerializer
from rest_framework.exceptions import ValidationError
from rest_framework.serializers import raise_errors_on_nested_writes

from sososa.models import Race, Team, influx_client, create_tslocation


def validate_team_id(value):
    if not Team.objects.filter(pk=value).exists():
        raise ValidationError('team_id {} not exist'.format(value),
                              code='team-not-exist')


class LocationMiniSerializer(Serializer):
    latitude = serializers.FloatField(required=False)
    longitude = serializers.FloatField(required=False)
    speed = serializers.FloatField(required=False)


class RaceSerializer(ModelSerializer):
    class Meta:
        model = Race
        fields = '__all__'


class TeamSerializer(ModelSerializer):
    class Meta:
        model = Team
        fields = ('name', 'color')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.context.get('with_location'):
            self.fields['location'] = \
                LocationMiniSerializer(source='current_location')


class LocationSerializer(LocationMiniSerializer):
    team_id = serializers.IntegerField(validators=[validate_team_id])

    def create(self, validated_data):
        raise_errors_on_nested_writes('create', self, validated_data)
        data = {}
        for k, v in validated_data.items():
            data[k] = v
        t = data.pop('time', timezone.localtime())
        team_id = data.pop('team_id')
        point = create_tslocation(team_id, t, **data)
        success = influx_client.write_points((point,))
        if success:
            return validated_data

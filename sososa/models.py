from django.db import models
from django.conf import settings
from django.db.models import CASCADE
from django.db.models import CharField, ManyToManyField, ForeignKey, FloatField, \
    DurationField, OneToOneField, DateTimeField
from django.utils import timezone
from colorful.fields import RGBColorField

from project.utils import get_influx


influx_client = get_influx()


class Race(models.Model):
    name = CharField(max_length=255, default='Amazing race')
    starting_latitude = FloatField(default=10.3481064)
    starting_longitude = FloatField(default=107.0914068)
    destination_latitude = FloatField(default=10.3481064)
    destination_longitude = FloatField(default=107.0914068)
    speed_limit = FloatField(null=True, blank=True, help_text='Speed limit in m/s')
    start_at = DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Team(models.Model):
    name = CharField(max_length=200, blank=True, default='Team')
    color = RGBColorField(null=True, blank=True)
    race = ForeignKey(Race, on_delete=CASCADE, related_name='teams')
    members = ManyToManyField(settings.AUTH_USER_MODEL, through='Membership',
                              related_name='teams')

    def __str__(self):
        return self.name

    @property
    def current_location(self):
        earliest = timezone.localtime()
        q = "SELECT * FROM location WHERE team_id = '{}' AND time >= '{}' \
            ORDER BY time DESC LIMIT 1".format(self.id, earliest.isoformat('T'))
        return next(influx_client.query(q).get_points(), None)


class Membership(models.Model):
    user = ForeignKey(settings.AUTH_USER_MODEL, on_delete=CASCADE)
    team = ForeignKey(Team, on_delete=CASCADE)


##
# Location is saved in InfluxDB with these fields
#     - latitude
#     - longitude
#     - speed
#     - foul
# and tags
#     - team_id


class Obstacle(models.Model):
    race = ForeignKey(Race, on_delete=CASCADE, related_name='obstacles', null=True)
    latitude = FloatField(default=10.3481064)
    longitude = FloatField(default=107.0914068)
    description = models.TextField(null=True, blank=True)


class HiddenTreasure(models.Model):
    race = ForeignKey(Race, on_delete=CASCADE, related_name='hidden_treasures', null=True)
    latitude = FloatField(default=10.3481064)
    longitude = FloatField(default=107.0914068)
    description = models.TextField(null=True, blank=True)


def create_tslocation(team_id=None, date_measured=None, **kwargs):
    return {
        'measurement': 'location',
        'tags': {'team_id': team_id},
        'time': date_measured,
        'fields': kwargs
    }

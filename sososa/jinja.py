import platform

import ujson
from jinja2.ext import Extension
from django.conf import settings
from django.templatetags.l10n import localize
from django.templatetags.tz import localtime
from django.utils.translation import get_language_info
from django.template.defaultfilters import date as date_filter
from django.contrib.messages import constants as DEFAULT_MESSAGE_LEVELS
from django.urls.base import resolve


MESSAGE_LEVEL_CLASSES = {
    DEFAULT_MESSAGE_LEVELS.DEBUG: "alert alert-warning",
    DEFAULT_MESSAGE_LEVELS.INFO: "alert alert-info",
    DEFAULT_MESSAGE_LEVELS.SUCCESS: "alert alert-success",
    DEFAULT_MESSAGE_LEVELS.WARNING: "alert alert-warning",
    DEFAULT_MESSAGE_LEVELS.ERROR: "alert alert-danger",
}


def tojson(value):
    return ujson.dumps(value)


def startswith(value: str, substr: str) -> bool:
    return value.startswith(substr)


def bootstrap_message_classes(message):
    """
    Return the message classes for a message
    """
    extra_tags = None
    try:
        extra_tags = message.extra_tags
    except AttributeError:
        pass
    if not extra_tags:
        extra_tags = ""
    classes = [extra_tags]
    try:
        level = message.level
    except AttributeError:
        pass
    else:
        try:
            classes.append(MESSAGE_LEVEL_CLASSES[level])
        except KeyError:
            classes.append("alert alert-danger")
    return ' '.join(classes).strip()


def resolve_tag(url):
    return resolve(url).url_name


class MyExtension(Extension):
    def __init__(self, environment):
        super().__init__(environment)
        environment.filters.update({
            'tojson': tojson,
            'startswith': startswith,
            'bootstrap_message_classes': bootstrap_message_classes,
            'date': date_filter,
            'localize': localize,
            'localtime': localtime,
            'resolve': resolve_tag,
        })
        environment.globals.update({
            'hostname': platform.node(),
            'get_language_info': get_language_info,
        })

from django.contrib import admin

from .models import Race, Team, Membership, Obstacle, HiddenTreasure

@admin.register(Race)
class RaceAdmin(admin.ModelAdmin):
    list_display = ('name', 'starting_latitude', 'starting_longitude',
                    'destination_latitude', 'destination_longitude',
                    'speed_limit', 'start_at')


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'race')
    list_filter = ('race',)


@admin.register(Obstacle)
class ObstacleAdmin(admin.ModelAdmin):
    list_display = ('id', 'race', 'latitude', 'longitude')
    list_filter = ('race',)


@admin.register(HiddenTreasure)
class HiddenTreasureAdmin(admin.ModelAdmin):
    list_display = ('id', 'race', 'latitude', 'longitude')
    list_filter = ('race',)

admin.site.register(Membership)

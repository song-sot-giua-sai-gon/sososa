from datetime import datetime
from logging import getLogger

from django.conf import settings
from django.contrib import messages
from django.utils.html import format_html
from django.templatetags.l10n import localize
from django.utils.translation import gettext_lazy as _

from .utils import get_source_version

logger = getLogger(__name__)
VERSION = get_source_version()


def add_domain_changing_message(request):
    '''
    Add message to inform user about moving to new domain.

    This "context processor" approach is chosen over "middleware" to fix the issue of duplicate message,
    due to the fact that, middleware is run even with 302 (redirect) response.
    '''
    new_domain = settings.NEW_DOMAIN.strip()
    old_domain_expiry = settings.OLD_DOMAIN_EXPIRY
    if new_domain == '' or old_domain_expiry == '':
            return {}

    current_domain = request.get_host().split(':')[0]
    if current_domain == new_domain:
        return {}
    try:
        FORMAT = '%Y-%m-%d'
        expiry_date = datetime.strptime(old_domain_expiry, FORMAT)
    except ValueError:
        logger.error('OLD_DOMAIN_EXPIRY (%s) cannot be parsed! Must be in %s format.',
                     old_domain_expiry, FORMAT)
        return {}

    # create inform message
    new_domain_link = format_html("<a href='http://{}'>{}</a>", new_domain, new_domain)
    content = _("This website is moving to new domain %(new_domain_link)s."
                " This domain %(current_domain)s will stop working on %(expiry_date)s.") % {
        'new_domain_link': new_domain_link,
        'current_domain': current_domain,
        'expiry_date': localize(expiry_date.date())
    }
    messages.info(request, content)
    return {}


def static_revision(request):
    return {'REVISION': VERSION}

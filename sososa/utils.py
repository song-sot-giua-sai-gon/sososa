import logging
import subprocess
from pathlib import Path


logger = logging.getLogger(__name__)


def get_source_version_from_git():
    path = Path(__file__, '../../.git').resolve()
    if not path.is_dir():
        raise IOError('Not a Git working copy')
    try:
        return subprocess.check_output(('git', 'log', '-1', '--format=%h'),
                                       timeout=1, encoding='utf-8')
    except (subprocess.CalledProcessError, subprocess.TimeoutExpired):
        logger.error('Failed to get version from Git')
        raise IOError('Calling Git failed')


def get_source_version_from_text_file():
    path = Path(__file__, '../../.version').resolve()
    try:
        with open(path) as f:
            content = f.read().strip()
    except IOError:
        logger.error(f'Failed to read {path}')
        return 'version-file-missing'
    return content


def get_source_version():
    try:
        return get_source_version_from_git()
    except IOError:
        return get_source_version_from_text_file()

const pages = {
  'room-detail': {
    entry: './src/room-detail.js',
  },
  'node-detail': './src/node-detail.js',
  'room-control': './src/room-control.js',
  'dashboard': './src/dashboard.js'
};

module.exports = {
	outputDir: '../frontend-compiled/webpack',
	baseUrl: '/static/webpack',
	pages: pages,
	css: {
		extract: {
			filename: '[name].css',
		}
	},
	configureWebpack: {
		output: {
			filename: '[name].js',
			chunkFilename: '[name].js',
		},
		externals: {
			// Don't include Vue in vendor file.
			vue: 'Vue'
		},
		resolve: {
			alias: {
				// Leave empty because we load VueJS via <script>
			}
		},
	},
	chainWebpack: config => {
		Object.keys(pages).forEach(page => {
			config.plugins.delete(`html-${page}`);
			config.plugins.delete(`preload-${page}`);
			config.plugins.delete(`prefetch-${page}`);
		});
		config.plugin('define').tap(args => {
			/* Default args is
			 * [ { 'process.env': { NODE_ENV: '"production"', BASE_URL: '"/static/webpack/"' } } ]
			 */
			// Add "PRODUCTION" to modify "Vue.config.devtools" in our JS scripts.
			args[0].PRODUCTION = JSON.stringify(process.env.NODE_ENV === 'production');
			return args;
		});
	}
}
